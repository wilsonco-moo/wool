/*
 * WindowConfig.h
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#ifndef OPENGLUTIL_CONFIG_WINDOWCONFIG_H_
#define OPENGLUTIL_CONFIG_WINDOWCONFIG_H_


namespace wool {

    /**
     * This class stores all the config required to create an opengl glut window.
     */
    class WindowConfig {
    public:

        /**
         * The display mode used with glutInitDisplayMode.
         * Default is GLUT_DOUBLE.
         */
        unsigned int displayMode;

        /**
         * The width and height of the glut window, used with glutInitWindowSize.
         * Default is 800x600.
         */
        int width, height;

        /**
         * The x and y position of the window to start off with, used in glutInitWindowPosition.
         * Default is 100, 100.
         */
        int x, y;

        /**
         * The default window title, used in glutCreateWindow.
         * Default is "Wilson's wizard for openGL".
         */
        char * windowTitle;

        /**
         * If set to true, this will use nasty windoze-specific stuff to enable vsync. If this is not set,
         * windoze will run the game at the maximum frame-rate possible, causing very high GPU and CPU usage.
         * In linux, double buffering of the window seems to sync to the screens refresh rate anyway,
         * so this option will make no difference. MacOS!? who knows.
         * The method for enabling vsync is implemented in WindowBase.cpp, using a method from https://stackoverflow.com/a/589232.
         * Default is true.
         */
        bool enableWindozeVsync;

        WindowConfig(void);
        virtual ~WindowConfig(void);
    };

}





#endif
