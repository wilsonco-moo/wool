/*
 * WindowConfig.cpp
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#include "../../../wool/window/config/WindowConfig.h"

#include <GL/freeglut_std.h>

namespace wool {

    WindowConfig::WindowConfig(void) :
        displayMode(GLUT_DOUBLE | GLUT_RGBA),
        width(800),
        height(600),
        x(100),
        y(100),
        windowTitle((char *)"Wilson's wizard for openGL"),
        enableWindozeVsync(true) {
    }

    WindowConfig::~WindowConfig(void) {}

}
