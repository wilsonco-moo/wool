/*
 * WindowBase.cpp
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#include "WindowBase.h"

#include <GL/freeglut_std.h> // *** THIS MUST BE INCLUDED FIRST  ***
#include <GL/freeglut_ext.h> // *** THIS MUST BE INCLUDED SECOND ***
#include <math.h>
#include <stddef.h>
#include <cmath>
#include <iostream>
#include <cstdlib>


// For windoze-specific vsync.
#ifdef _WIN32
    #include <windows.h>
    #include <GL/wglext.h>
#endif



namespace wool {



    // ================================ WINDOZE SPECIFIC VSYNC IMPLEMENTATION ============================================

    #ifdef _WIN32
        int WGLExtensionSupported(const char *extension_name);
        /*
         * This function enables vsync.
         * In windoze, this uses the nasty windoze-specific stuff to enable vsync. (https://stackoverflow.com/a/589232)
         * In linux, double buffering seems to sync to the screens refresh rate anyway, so this does nothing.
         * MacOS ??
         */
        void enableWindozeSpecificVsync(void) {
            PFNWGLSWAPINTERVALEXTPROC       wglSwapIntervalEXT    = NULL;
            PFNWGLGETSWAPINTERVALEXTPROC    wglGetSwapIntervalEXT = NULL;
            if (WGLExtensionSupported("WGL_EXT_swap_control")) {
                // Extension is supported, init pointers.
                wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC) wglGetProcAddress("wglSwapIntervalEXT");
                // this is another function from WGL_EXT_swap_control extension
                wglGetSwapIntervalEXT = (PFNWGLGETSWAPINTERVALEXTPROC) wglGetProcAddress("wglGetSwapIntervalEXT");
                wglSwapIntervalEXT(1);
            }
        }
        /*
         * This is a helper function for the windoze vsync function.
         */
        int WGLExtensionSupported(const char *extension_name) {
            // this is pointer to function which returns pointer to string with list of all wgl extensions
            PFNWGLGETEXTENSIONSSTRINGEXTPROC _wglGetExtensionsStringEXT = NULL;
            // determine pointer to wglGetExtensionsStringEXT function
            _wglGetExtensionsStringEXT = (PFNWGLGETEXTENSIONSSTRINGEXTPROC) wglGetProcAddress("wglGetExtensionsStringEXT");
            if (strstr(_wglGetExtensionsStringEXT(), extension_name) == NULL) {
                // string was not found
                return 0;
            }
            // extension is supported
            return 1;
        }
    #endif

    // ===================================================================================================================





    // Initialise the value of mainWindowBase. Initially this is NULL.
    WindowBase * WindowBase::mainWindowBase = NULL;

    // Stuff to interface directly with glut.
    // Here we need some static functions to use as pointers with glut.

    void WindowBase::windowBaseInit(void) {
        mainWindowBase->currentRoom->init();
    }
    void WindowBase::windowBaseStep(void) {
        // Each frame, first run the time calculation stuff.
        // This must happen first, to make the timing as accurate as possible.
        mainWindowBase->manageFrameRate();
        // Next check for any requests for room changes. This should be done first, as otherwise
        // view area reshaping would happen at the end of a frame, causing the last frame of the previous
        // room to appear using the view of the next room.
        mainWindowBase->checkForRoomChangeAndReshape();
        // Now check for exiting the program. This must be done after checkForRoomChangeAndReshape(), as otherwise
        // changing the room, then exiting would cause the program to dump the prospective new room.
        if (mainWindowBase->checkForExit()) {
            // Finally, run the current room's step method, but only if we have not exited.
            mainWindowBase->currentRoom->step();
        }
    }
    void WindowBase::windowBaseReshape(GLsizei width, GLsizei height) {
        if (!mainWindowBase->shouldExitLater) {
            mainWindowBase->lastWidth = width;
            mainWindowBase->lastHeight = height;
            mainWindowBase->currentRoom->reshape(width, height);
        }
    }


    // =============== KEYBOARD AND MOUSE EVENTS =============================

    // Note: All of these events are disabled if prospectiveNewRoom is not NULL.
    // This ensures the current room DOES NOT RECEIVE keyboard OR mouse events
    // after calling windowBase->changeRoom().

    void WindowBase::windowBaseKeyNormal(unsigned char key, int x, int y) {
        if (mainWindowBase->prospectiveNewRoom == NULL && !mainWindowBase->shouldExitLater) mainWindowBase->currentRoom->keyNormal(key, x, y);
    }
    void WindowBase::windowBaseKeyNormalRelease(unsigned char key, int x, int y) {
        if (mainWindowBase->prospectiveNewRoom == NULL && !mainWindowBase->shouldExitLater) mainWindowBase->currentRoom->keyNormalRelease(key, x, y);
    }
    void WindowBase::windowBaseKeySpecial(int key, int x, int y) {
        if (mainWindowBase->prospectiveNewRoom == NULL && !mainWindowBase->shouldExitLater) mainWindowBase->currentRoom->keySpecial(key, x, y);
    }
    void WindowBase::windowBaseKeySpecialRelease(int key, int x, int y) {
        if (mainWindowBase->prospectiveNewRoom == NULL && !mainWindowBase->shouldExitLater) mainWindowBase->currentRoom->keySpecialRelease(key, x, y);
    }
    void WindowBase::windowMouseEvent(int button, int state, int x, int y) {
        if (mainWindowBase->prospectiveNewRoom == NULL && !mainWindowBase->shouldExitLater) mainWindowBase->currentRoom->mouseEvent(button, state, x, y);
    }
    void WindowBase::windowMouseMove(int x, int y) {
        if (mainWindowBase->prospectiveNewRoom == NULL && !mainWindowBase->shouldExitLater) mainWindowBase->currentRoom->mouseMove(x, y);
    }
    void WindowBase::windowMouseDrag(int x, int y) {
        if (mainWindowBase->prospectiveNewRoom == NULL && !mainWindowBase->shouldExitLater) mainWindowBase->currentRoom->mouseDrag(x, y);
    }








    WindowBase::WindowBase(RoomBase * startRoom, WindowConfig config) :
        prospectiveNewRoom(NULL),
        shouldExitLater(false),
        shouldReshape(false),
        lastWidth(0.0f),
        lastHeight(0.0f),
        currentRoom(startRoom),
        timeElapsed(1.0f/60.0f), // Set the initial frame time elapsed to one sixtieth of a second, as if we have been running at 60fps.
        timeFps(60.0f),          // Set the initial frame rate to 60fps. The initial values of timeFps and timeElapsed will avoid zero values here for the initialisers of rooms.
                                 // Depending on room logic, zero values may cause avoidable divide-by-zero errors.
        timeFrameCount(0),
        lastStepTime(0),
        fpsCalcCounter(0.0f),
        fpsCalcElapsed(0.0f),
        fpsFrameCount(0),
        enableFrameRateLogging(false),
        config(config) {

        mainWindowBase = this;
    }

    WindowBase::WindowBase(RoomBase * startRoom) : WindowBase(startRoom, WindowConfig()) {}

    WindowBase::~WindowBase(void) {
        // Make sure the current room is definately destroyed when we exit.
        if (currentRoom != NULL && currentRoom->shouldDeleteOnRoomEnd()) {
            delete currentRoom;
        }
    }

    void WindowBase::start(int * argc, char **argv) {

        glutInit(argc, argv);            // Run glutInit
        glutInitDisplayMode(config.displayMode); // Set the display mode to double buffering.
        glutInitWindowSize(config.width, config.height);     // Set the window size.
        glutInitWindowPosition(config.x, config.y); // Set the window position.
        glutCreateWindow(config.windowTitle);      // Set the window title.

        // Set the option so that when we exit, glutMainLoop actually returns.
        glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);

        // Now we should set up function pointers for the window:

        glutDisplayFunc(&windowBaseStep); // Set the display function: The function to redraw the window.
        glutIdleFunc(&windowBaseStep); // Set the idle function: This runs each frame.
        glutReshapeFunc(&windowBaseReshape); // Set the reshape function: This runs each time the window is resized.

        // After that we should set more function pointers:

        glutKeyboardFunc(&windowBaseKeyNormal); // This runs each time a key is pressed.
        glutKeyboardUpFunc(&windowBaseKeyNormalRelease); // This runs each time a key is released.
        glutSpecialFunc(&windowBaseKeySpecial); // This runs each time a special key is pressed.
        glutSpecialUpFunc(&windowBaseKeySpecialRelease); // This runs each time a special key is released.

        // And the mouse function pointers:
        glutMouseFunc(&windowMouseEvent);
        glutPassiveMotionFunc(&windowMouseMove);
        glutMotionFunc(&windowMouseDrag);

        // Next we should set some opengl options

        // Enable blending.
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);

        // Set texturing parameters to allow alpha
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);



        // Finally we should start the game.

        // So set up the circular pointer
        currentRoom->windowBase = this;
        // Then run the init function of the current room.
        windowBaseInit();

        // Next, if we are in windoze, and the config says that we should enable windoze-specific vsync, then do so.
        #ifdef _WIN32
            if (config.enableWindozeVsync) {
                enableWindozeSpecificVsync();
            }
        #endif

        glutMainLoop(); // Finally start the glut main program loop.
    }

    void WindowBase::requestReshape(void) {
        shouldReshape = true;
    }

    RoomBase * WindowBase::getCurrentRoom(void) const {
        return currentRoom;
    }


    void WindowBase::exitLater(void) {
        shouldExitLater = true;
    }



    // ----------- ROOM CHANGING -------------

    void WindowBase::changeRoom(RoomBase * newRoom) {
        prospectiveNewRoom = newRoom;
    }
    void WindowBase::checkForRoomChangeAndReshape(void) {
        if (prospectiveNewRoom != NULL) {
            // Run the room's end method.
            currentRoom->end();
            // If we should delete the room, delete it.
            if (currentRoom->shouldDeleteOnRoomEnd()) {
                delete currentRoom;
            }
            // Set our current room to the new one.
            currentRoom = prospectiveNewRoom;
            // Nullify the prospective new room pointer.
            prospectiveNewRoom = NULL;
            // Then set their windowBase pointer to us.
            currentRoom->windowBase = this;
            // Finally run the init method on our new room.
            currentRoom->init();
            // And ensure that we reshape in the next step.
            shouldReshape = true;
        }
        if (shouldReshape) {
            currentRoom->reshape(lastWidth, lastHeight);
            shouldReshape = false;
        }
    }
    bool WindowBase::checkForExit(void) {
        if (shouldExitLater) {
            // Delete the current room if they want us to,
            if (currentRoom->shouldDeleteOnRoomEnd()) {
                delete currentRoom;
                currentRoom = NULL; // Set to NULL to avoid the user accidentally using the room after deleting it.
            }
            // If in windows, just do a dumb process exit, since windows doesn't seem to cope
            // with the glutLeaveMainLoop function.
            #ifdef _WIN32
                exit(EXIT_SUCCESS);
            #else
                // In UNIX/Linux run glutLeaveMainLoop to safely and cleanly exit.
                glutLeaveMainLoop();
            #endif

            // Return false since we don't want to continue to do anything this step.
            return false;
        }
        return true;
    }




    // --------------------------------- TIME CALCULATION ------------------------------------




    /**
     * This is the private method run at the start of each step to update the frame-rate stuff.
     */
    void WindowBase::manageFrameRate(void) {

        // Get the time elapsed in milliseconds since the start of the game, using glutGet.
        int thisStepTime = glutGet(GLUT_ELAPSED_TIME);

        if (timeFrameCount == 0) {
            // For the first step, set the elapsed time to 1/60th of a second.
            // Otherwise, the elapsed time would cover the time taken for the possibly very lengthy initialisation
            // stage of the game. That would not be desirable.
            timeElapsed = 1.0f/60.0f;
        } else {
            // For all other frames:
            // Work out the elapsed time since the previous step, divide by 1000 to convert to seconds.
            timeElapsed = ((GLfloat)(thisStepTime - lastStepTime)) / 1000.0f;
        }


        // Add the elapsed time onto the frames-per-second counters.
        fpsCalcCounter += timeElapsed; // Used to work out when to do a frames-per-second calculation.
        fpsCalcElapsed += timeElapsed; // Used as the reference value of the time elapsed in this second.
                                       // Note: This will not always be 1 second, as it will be to the closest frame.

        // If at least 1 second has passed since we last did a calculation:
        if (fpsCalcCounter >= 1.0f) {
            // Work out the frame rate as the frames that have happened in this second, divided by the actual time that has elapsed.
            timeFps = ((GLfloat)fpsFrameCount)/fpsCalcElapsed;
            // Reset the frame counter.
            fpsFrameCount = 0;
            // Modulus the variable that is used to work out when to do a frames-per-second calculation, by 1.
            // Doing this rather than setting it to zero means that a calculation will always be done n times in n
            // seconds, rather than being the frame that happens immediately after a second has passed - (which would
            // usually be at "just over one second" intervals).
            fpsCalcCounter = fmod(fpsCalcCounter, 1.0f);
            // Reset the variable used as the reference value of the time elapsed in this second, to zero. This must
            // be set to zero, as this must work out the exact time interval between each calculation. This should not
            // be offset by the nearest frame time (like fpsCalcCounter), as otherwise it would not be the exact time
            // elapsed, so would not be the correct value to use in the calculation of timeFps.
            fpsCalcElapsed = 0.0f;
            // If frame rate logging is enabled, print the frame rate.
            if (enableFrameRateLogging) {
                std::cout << timeFps << " fps\n";
            }
        }

        // Finally, set the last step time to this step time. This is not done by calling glutGet(GLUT_ELAPSED_TIME)
        // again, as otherwise the time this code takes to execute would be missed, and the elapsed time calculation
        // would be slightly too small.
        lastStepTime = thisStepTime;
        // Increment the global frame count.
        timeFrameCount++;
        // Increment the frame count used in frames-per-second calculation.
        fpsFrameCount++;
    }

}
