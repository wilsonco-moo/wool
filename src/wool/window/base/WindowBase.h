/*
 * WindowBase.h
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#ifndef WOOL_WINDOW_BASE_WINDOWBASE_H_
#define WOOL_WINDOW_BASE_WINDOWBASE_H_


#include <GL/gl.h>

#include "../../../wool/room/base/RoomBase.h"
#include "../../../wool/window/config/WindowConfig.h"

namespace wool {

    /**
     * This should be sub-classed to provided a basis for a windowed glut application.
     * WindowBase is a RoomBase, so subclassing WindowBase directly allows only one room.
     *
     * Note: Due to limitations of glut, there can only be one instance of WindowBase per
     * running instance of a program.
     */
    class WindowBase {


    private:
        // These are used to change rooms at a later time.
        RoomBase * prospectiveNewRoom;

        void checkForRoomChangeAndReshape(void);

        // These are used to exit at a later point in time.
        bool shouldExitLater;
        // This returns true if we should continue running the stuff in this step.
        bool checkForExit(void);



        // A pointer to the single active WindowBase instance, so it can be found
        // by the following static functions.
        static WindowBase * mainWindowBase;

        // -------- The functions to give to glut, so they must be static ----------

        static void windowBaseInit(void);
        static void windowBaseStep(void);
        static void windowBaseKeyNormal(unsigned char key, int x, int y);
        static void windowBaseKeyNormalRelease(unsigned char key, int x, int y);
        static void windowBaseKeySpecial(int key, int x, int y);
        static void windowBaseKeySpecialRelease(int key, int x, int y);
        static void windowMouseEvent(int button, int state, int x, int y);
        static void windowMouseMove(int x, int y);
        static void windowMouseDrag(int x, int y);
        static void windowBaseReshape(GLsizei width, GLsizei height);


        // These are used to allow rooms to request a reshape, and to run the reshape on new rooms.
        bool shouldReshape;
        GLsizei lastWidth, lastHeight;


        // This stores the currently active room.
        RoomBase * currentRoom;



        // ----------------- Private time variables and methods  ---------

        // Variables returned by public getter methods.
        GLfloat timeElapsed, // Stores the current elapsed time since the previous game step.
                timeFps;     // Stores the current game frame rate.
        int timeFrameCount;  // Stores the number of frames that have happened since the game was launched

        int lastStepTime;       // The time (in milliseconds since the game started) that the previous frame happened. This is used to calculate timeElapsed.
        GLfloat fpsCalcCounter; // timeElapsed is added to this each second. This is used to keep track of WHEN TO DO a frames-per-second calculation.
        GLfloat fpsCalcElapsed; // timeElapsed is added to this each second. This is used in the frames-per-second calculation as the time elapsed in that second.
        int fpsFrameCount;      // One is added to this each second. This is used to keep track of the number of frames that have happened in the time elapsed since the last frames-per-second calculation.

        // This is called at the start of each game step. This updates all the variables above.
        void manageFrameRate(void);


        // ---------------------------------------------------------------


    public:

        /**
         * This controls whether a message saying: "46.4324 fps" (or similar) is printed each second.
         * The default value for this is false.
         */
        bool enableFrameRateLogging;


        // This stores all the view configuration for this window.
        WindowConfig config;

        /**
         * A WindowConfig can be supplied, otherwise default options will be used.
         */
        WindowBase(RoomBase * startRoom, WindowConfig config);
        WindowBase(RoomBase * startRoom);
        virtual ~WindowBase(void);

        /**
         * This method should be run to start glut.
         */
        void start(int * argc, char **argv);


        /**
         * This method can be called at any point to change to a new room.
         * The change will occur at the next possible opportunity.
         *
         * Important information about behaviour of keyboard/mouse events when rooms are changed:
         *
         *     > After calling changeRoom, it is ensured that the old room DOES NOT RECEIVE
         *       any further keyboard/mouse events.
         *
         *     > NOT ALL keyboard/mouse events will be RELIABLY CONVEYED, ON THE FRAME THAT ROOMS
         *       ARE CHANGED, since after calling this, for one frame all keyboard/mouse events are ignored.
         */
        void changeRoom(RoomBase * newRoom);


        /**
         * This should be called by rooms if they change any of their view options.
         * This will run the room's reshape function at the next convenient opportunity.
         */
        void requestReshape(void);

        /**
         * Returns the currently active room.
         * This should be used with caution, and the value should not be saved.
         * This is because if the room changes, this will no longer point to the
         * current room. If the room returns true on shouldDeleteOnRoomEnd(), then this
         * old room may have been removed from memory entirely.
         */
        RoomBase * getCurrentRoom(void) const;


        /**
         * This will cleanly exit from wool/GLUT, at the next convenient opportunity. This will
         * delete the current room if specified by the room. This will also block all further keyboard
         * and mouse events, after this call has been made.
         *
         * Exiting is eventually done by calling glutLeaveMainLoop(), next time we start a step.
         * This method can be called at any point from within the GLUT thread.
         *
         * NOTE: Exiting may not happen until the start of the next step.
         *
         * This method should be called instead of calling glutLeaveMainLoop() manually, as this method
         * will properly clean up wool and the room system.
         */
        void exitLater(void);


        // ------------------------------- PUBLIC TIME GETTER METHODS ----------------------------

        /**
         * Gets the elapsed time (in seconds) since the previous frame.
         * This will return 1/60th of a second in the init method of the first room, and the first step of the game.
         * After that values will be completely accurate.
         */
        inline GLfloat elapsed(void) const {
            return timeElapsed;
        }
        /**
         * Gets the current frame rate.
         * This will return 60.0 as a default value in the first second of the game, before the frame rate has actually been calculated.
         */
        inline GLfloat fps(void) const {
            return timeFps;
        }
        /**
         * Gets the number of frames that have happened since the game has started.
         * This will return zero in the init method of the first frame, 1 on the first step call, 2 on the second step call etc.
         */
        inline int frameCount(void) const {
            return timeFrameCount;
        }



    };

}






#endif
