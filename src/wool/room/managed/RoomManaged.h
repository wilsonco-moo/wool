/*
 * RoomManaged.h
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#ifndef WOOL_ROOM_MANAGED_ROOMMANAGED_H_
#define WOOL_ROOM_MANAGED_ROOMMANAGED_H_

#include <GL/gl.h>

#include "../../../wool/misc/RGBA.h"
#include "../../../wool/room/base/RoomBase.h"


namespace wool {


    /**
     * These are the different view modes which can be used with RoomManaged.
     */
    enum ViewModes {
        /**
         * Stretch mode stretches the view area horizontally and vertically, and ignores the aspect ratio.
         */
        viewModeStretch,
        /**
         * Aspect mode scales the view area to keep it's aspect ratio the same, while fitting it into the window's area.
         * This is the default view mode.
         */
        viewModeAspect,
        /**
         * Extend mode extends the view area, so the view area becomes larger in size as the window becomes larger in size.
         */
        viewModeExtend
    };



    /**
     * RoomViewOptions stores all the configuration for the room's view area's width, height, scale and borders.
     * If any of these are modified, then windowBase->requestReshape() should be called, otherwise the view
     * will not change until the window is resized.
     */
    struct RoomViewOptions {
        /**
         * The current view mode being used. Can be viewModeStretch, viewModeAspect or viewModeExtend.
         * The default view mode is viewModeAspect.
         */
        ViewModes viewMode;
        /**
         * The width of the view area. This is used in viewModeStretch and viewModeAspect.
         */
        GLfloat width;
        /**
         * The height of the view area. This is used in viewModeStretch and viewModeAspect.
         */
        GLfloat height;
        /**
         * The top view area border. This is used in all scale modes.
         */
        GLfloat borderTop;
        /**
         * The left view area border. This is used in all scale modes.
         */
        GLfloat borderLeft;
        /**
         * The bottom view area border. This is used in viewModeStretch and viewModeAspect.
         */
        GLfloat borderBottom;
        /**
         * The right view area border. This is used in viewModeStretch and viewModeAspect.
         */
        GLfloat borderRight;
        /**
         * This is the relative x-scale for the view area. This is only used in viewModeExtend.
         */
        GLfloat xScale;
        /**
         * This is the relative y-scale for the view area. This is only used in viewModeExtend.
         */
        GLfloat yScale;
    };







    /**
     * The managed room adds extra features to rooms,
     * such as:
     *     > Automated step stuff
     *     > Automatic resizing, view modes and scaling maths
     */
    class RoomManaged : public RoomBase {
    public:
        RoomManaged(void);
        RoomManaged(struct RoomViewOptions viewOptions);
        virtual ~RoomManaged(void);

        virtual void step(void) override final;
        virtual void reshape(GLsizei width, GLsizei height) override final;


        // ---------------------------- PUBLIC COORDINATE CONVERSION METHODS ------------------------

        /**
         * These four methods convert between window coordinates and view coordinates.
         */
        GLfloat windowXToViewX(int x) const;
        GLfloat windowYToViewY(int y) const;
        int viewXToWindowX(GLfloat x) const;
        int viewYToWindowY(GLfloat y) const;

        // -----------------------------------------------------------------------------------------

    protected:

        /**
         * This is the background colour for the room, i.e: The colour filled in each frame.
         * The default is a sort of grey-ish colour: RGBA(0.4f, 0.4f, 0.4f, 1.0f).
         */
        RGBA background;

        struct RoomViewOptions viewOptions;

        /**
         * For managed rooms, the step function is replaced
         * with onStep. This is so the managed room can perform extra tasks.
         */
        virtual void onStep(void) = 0;




        /**
         * These methods can be used to get the current draw area, so gradiented backgrounds can be drawn for example.
         */
        inline GLfloat getViewCurrentMinX(void) const { return viewCurrentMinX; }
        inline GLfloat getViewCurrentMinY(void) const { return viewCurrentMinY; }
        inline GLfloat getViewCurrentMaxX(void) const { return viewCurrentMaxX; }
        inline GLfloat getViewCurrentMaxY(void) const { return viewCurrentMaxY; }

        /**
         * These methods get the current width and height of the view area.
         */
        inline GLfloat getViewCurrentWidth(void) const { return viewCurrentWidth; }
        inline GLfloat getViewCurrentHeight(void) const { return viewCurrentHeight; }

        /**
         * These methods can get the actual width and height on screen the game window currently occupies,
         * in window coordinates.
         */
        inline GLsizei getWindowCurrentWidth(void) const { return windowCurrentWidth; }
        inline GLsizei getWindowCurrentHeight(void) const { return windowCurrentHeight; }




    private:

        // Private fields calculated by the reshaping.
        GLfloat viewCurrentMinX;
        GLfloat viewCurrentMinY;
        GLfloat viewCurrentMaxX;
        GLfloat viewCurrentMaxY;

        GLfloat viewCurrentWidth;
        GLfloat viewCurrentHeight;

        GLsizei windowCurrentWidth;
        GLsizei windowCurrentHeight;


        // Methods for resizing in specific circumstances:
        inline void managedRoomReshapeStretch(GLsizei width, GLsizei height);
        inline void managedRoomReshapeAspect(GLsizei width, GLsizei height);
        inline void managedRoomReshapeExtend(GLsizei width, GLsizei height);

    };

}









#endif
