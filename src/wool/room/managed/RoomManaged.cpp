/*
 * RoomManaged.cpp
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#include "../../../wool/room/managed/RoomManaged.h"

#include <GL/freeglut_std.h>
#include <GL/glu.h>
#include <iostream>


namespace wool {

    RoomManaged::RoomManaged(void) {
        background = RGBA(0.4f, 0.4f, 0.4f, 1.0f);

        viewOptions.viewMode = viewModeAspect; // Set the default view mode to viewModeExtend.
        viewOptions.width = 800.0f;
        viewOptions.height = 600.0f;
        viewOptions.borderTop = 0.0f;
        viewOptions.borderLeft = 0.0f;
        viewOptions.borderBottom = 0.0f;
        viewOptions.borderRight = 0.0f;
        viewOptions.xScale = 1.0f;
        viewOptions.yScale = 1.0f;

        viewCurrentMinX = 0.0f; viewCurrentMinY = 0.0f;
        viewCurrentMaxX = 0.0f; viewCurrentMaxY = 0.0f;

        windowCurrentWidth = 0; windowCurrentHeight = 0;
        viewCurrentWidth = 0.0f; viewCurrentHeight = 0.0f;
    }

    RoomManaged::RoomManaged(struct RoomViewOptions viewOptions) : viewOptions(viewOptions) {
        background = RGBA(0.4f, 0.4f, 0.4f, 1.0f);

        viewCurrentMinX = 0.0f; viewCurrentMinY = 0.0f;
        viewCurrentMaxX = 0.0f; viewCurrentMaxY = 0.0f;

        windowCurrentWidth = 0.0f; windowCurrentHeight = 0.0f;
        viewCurrentWidth = 0.0f; viewCurrentHeight = 0.0f;
    }

    RoomManaged::~RoomManaged(void) {
    }

    void RoomManaged::step(void) {

        // Fill with the default background colour.
        glClearColor(wool_getRGBA(background));
        // Clear the drawing area.
        glClear(GL_COLOR_BUFFER_BIT);

        // Run the room's step method.
        onStep();

        // Swap the buffers for double buffering.
        glutSwapBuffers();
    }

    void RoomManaged::reshape(GLsizei width, GLsizei height) {

        // Set the actual window size from the width and height provided.
        windowCurrentWidth = width;
        windowCurrentHeight = height;

        // Then do the appropriate resize based on the current view mode.
        switch(viewOptions.viewMode) {

        case viewModeStretch:
            managedRoomReshapeStretch(width, height);
        break;

        case viewModeAspect:
            managedRoomReshapeAspect(width, height);
        break;

        case viewModeExtend:
            managedRoomReshapeExtend(width, height);
        break;

        }
    }



    inline void RoomManaged::managedRoomReshapeStretch(GLsizei width, GLsizei height) {

        glViewport(0,0,width,height);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        viewCurrentMinX = -viewOptions.borderLeft;
        viewCurrentMaxX = viewOptions.width + viewOptions.borderRight;
        viewCurrentMinY = -viewOptions.borderTop;
        viewCurrentMaxY = viewOptions.height + viewOptions.borderBottom;

        viewCurrentWidth = viewCurrentMaxX - viewCurrentMinX;
        viewCurrentHeight = viewCurrentMaxY - viewCurrentMinY;
        gluOrtho2D(viewCurrentMinX,viewCurrentMaxX,viewCurrentMaxY,viewCurrentMinY);
    }

    inline void RoomManaged::managedRoomReshapeAspect(GLsizei width, GLsizei height){
        glViewport(0,0,width,height);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        GLfloat windowAspect = ((GLfloat)width)/((GLfloat)height),

                widthWithBorders = viewOptions.borderLeft + viewOptions.width + viewOptions.borderRight,
                heightWithBorders = viewOptions.borderTop + viewOptions.height + viewOptions.borderBottom,

                viewAspectWithBorders = widthWithBorders / heightWithBorders;

        if (windowAspect > viewAspectWithBorders) {
            GLfloat halfW = (windowAspect/viewAspectWithBorders)*0.5f;
            viewCurrentMinX = (0.5f - halfW)*widthWithBorders - viewOptions.borderLeft;
            viewCurrentMaxX = (halfW + 0.5f)*widthWithBorders - viewOptions.borderLeft;
            viewCurrentMinY = -viewOptions.borderTop;
            viewCurrentMaxY = viewOptions.height + viewOptions.borderBottom;
        } else {
            GLfloat halfH = (viewAspectWithBorders/windowAspect)*0.5f;
            viewCurrentMinY = (0.5f - halfH)*heightWithBorders - viewOptions.borderTop;
            viewCurrentMaxY = (halfH + 0.5f)*heightWithBorders - viewOptions.borderTop;
            viewCurrentMinX = -viewOptions.borderLeft;
            viewCurrentMaxX = viewOptions.width + viewOptions.borderRight;
        }

        viewCurrentWidth = viewCurrentMaxX - viewCurrentMinX;
        viewCurrentHeight = viewCurrentMaxY - viewCurrentMinY;
        gluOrtho2D(viewCurrentMinX,viewCurrentMaxX,viewCurrentMaxY,viewCurrentMinY);
    }

    inline void RoomManaged::managedRoomReshapeExtend(GLsizei width, GLsizei height){

        glViewport(0,0,width,height);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        viewCurrentMinX = -viewOptions.borderLeft;
        viewCurrentMaxX = width/viewOptions.xScale - viewOptions.borderLeft;
        viewCurrentMinY = -viewOptions.borderTop;
        viewCurrentMaxY = height/viewOptions.yScale - viewOptions.borderTop;

        viewCurrentWidth = viewCurrentMaxX - viewCurrentMinX;
        viewCurrentHeight = viewCurrentMaxY - viewCurrentMinY;
        gluOrtho2D(viewCurrentMinX,viewCurrentMaxX,viewCurrentMaxY,viewCurrentMinY);
    }




    // ------------------------------- POSITION CONVERSION --------------------------------------

    GLfloat RoomManaged::windowXToViewX(int x) const {
        return (((GLfloat)x) / ((GLfloat)windowCurrentWidth)) * viewCurrentWidth + viewCurrentMinX;
    }
    GLfloat RoomManaged::windowYToViewY(int y) const {
        return (((GLfloat)y) / ((GLfloat)windowCurrentHeight)) * viewCurrentHeight + viewCurrentMinY;
    }
    int RoomManaged::viewXToWindowX(GLfloat x) const {
        return (int)(((x - viewCurrentMinX) / viewCurrentWidth) * ((GLfloat)windowCurrentWidth));
    }
    int RoomManaged::viewYToWindowY(GLfloat y) const {
        return (int)(((y - viewCurrentMinY) / viewCurrentHeight) * ((GLfloat)windowCurrentHeight));
    }


}
