/*
 * RoomBase.h
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#ifndef WOOL_ROOM_BASE_ROOMBASE_H_
#define WOOL_ROOM_BASE_ROOMBASE_H_

#include <GL/gl.h>


namespace wool {

    // Forward declare WindowBase, to avoid circular dependency.
    class WindowBase;

    /**
     * This class represents a game room. This is a bare room interface, and should
     * only be sub-classed directly if it is required to override the frame-rate
     * and time calculations.
     */
    class RoomBase {

    private:

        // Ensure this is a friend class, so they can set our WindowBase field.
        friend class WindowBase;

        /**
         * This is a circularly dependent field, that points to our currently assigned WindowBase instance.
         */
        WindowBase * windowBase;

    public:
        RoomBase(void);
        virtual ~RoomBase(void);

        /**
         * This allows our window to be publicly accessible.
         * This is guaranteed to have a value when init() is called, but will have a value of NULL before that.
         */
        inline WindowBase * getWindowBase(void) const {
            return windowBase;
        }


        /**
         * This is run when the room is created.
         */
        virtual void init(void) = 0;

        /**
         * This is run each step in the room.
         */
        virtual void step(void) = 0;

        /**
         * This is run when we switch away from this room.
         * This will occur just before the destructor if shouldDeleteOnRoomEnd returns true.
         */
        virtual void end(void) = 0;

        /**
         * This is run each time a normal key is pressed. This is a: glutKeyboardFunc.
         */
        virtual void keyNormal(unsigned char key, int x, int y) = 0;

        /**
         * This is run each time a normal key is released. This is a: glutKeyboardUpFunc.
         */
        virtual void keyNormalRelease(unsigned char key, int x, int y) = 0;

        /**
         * This is run each time a special key is pressed. This is a: glutSpecialFunc.
         */
        virtual void keySpecial(int key, int x, int y) = 0;

        /**
         * This is run each time a special key is released. This is a: glutSpecialUpFunc.
         */
        virtual void keySpecialRelease(int key, int x, int y) = 0;

        /**
         * This is called each time the room is resized.
         * This is also called when we switch to this room, after init is called.
         */
        virtual void reshape(GLsizei width, GLsizei height) = 0;

        /**
         * This is called when a mouse event, such as a mouse click happens.
         * The mouse button can be any from GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, or GLUT_RIGHT_BUTTON.
         * The state can be any from GLUT_UP or GLUT_DOWN.
         */
        virtual void mouseEvent(int button, int state, int x, int y) = 0;

        /**
         * This is called when the mouse moves, while no mouse buttons are pressed.
         */
        virtual void mouseMove(int x, int y) = 0;

        /**
         * This is called when the mouse moves, while at least one mouse button is pressed.
         */
        virtual void mouseDrag(int x, int y) = 0;

        /**
         * This should return whether the room is deleted (freed from memory) when we switch to another
         * room.
         */
        virtual bool shouldDeleteOnRoomEnd(void) = 0;
    };

}



#endif
