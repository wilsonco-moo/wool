/*
 * RoomBase.cpp
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#include <cstddef>

#include "../../../wool/room/base/RoomBase.h"

namespace wool {

    RoomBase::RoomBase(void) :
		windowBase(NULL) {
    }

    RoomBase::~RoomBase(void) {
    }

}
