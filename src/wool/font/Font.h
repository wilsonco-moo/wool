/*
 * Font.h
 *
 *  Created on: 18 Jul 2018
 *      Author: wilson
 */

#ifndef WOOL_FONT_FONT_H_
#define WOOL_FONT_FONT_H_

#include <GL/gl.h>
#include <string>

namespace wool {





    /**
     * This is a direct port of the rectangle font system (originally written in C)
     * used in tatris2.1. As a result it may vomit out a few warnings if compiled with -Wall.
     *
     * The code has been modified (from the C version):
     *     > To run in a class
     *     > Many const keywords have been added
     *     > Two new functions taking std::string instead of unsigned char *
     *
     * NOTE: The font data supplied to this class SHOULD BE STATIC, as this class will
     *       make no attempt at memory managing it.
     */
    class Font {
    public:

        // ------------ Default pre-defined fonts -----------

        /**
         * A simple rectangle-based font.
         */
        static const Font font;
        /**
         * An alternate version of the font, for use when drawing very small text.
         */
        static const Font wideSpacedFont;

    private:
        const GLfloat * const fontData;

    public:

        /**
         * The main constructor requires a GLfloat array.
         */
        Font(const GLfloat * fontData);

        virtual ~Font();

        /**
         * Allows access to our internal font data.
         */
        inline const GLfloat * getData(void) const {
            return fontData;
        }


        // ---------------------------- Drawing methods -----------------------

        /**
         * Draws a single character to the screen's origin, at default scale.
         */
        void drawChar(char character) const;
        /**
         * Draws a single character to the screen, at the specified position, at default scale.
         */
        void drawCharPos(char character, GLfloat x, GLfloat y) const;
        /**
         * Draws a c-style string to the screen, at the specified location, at default scale.
         */
        void drawCString(const char * string, GLfloat x, GLfloat y) const;
        /**
         * Draws a c-style string to the screen, at the specified location, with a scaling
         * factor applied.
         */
        void drawCStringScale(const char * string, GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale) const;
        /**
         * Prints the config for this font, for debug reasons.
         */
        void printConfig(void) const;

        // -------------------- new C++ methods -------------------------------

        /**
         * This is the c++ replacement for drawCString, using std::string.
         * Draws a c++ style string to the screen, at the specified location, at default scale.
         */
        void drawString(const std::string & str, GLfloat x, GLfloat y) const;
        /**
         * This is the c++ replacement for drawCString, using std::string.
         * Draws a c++ style string to the screen, at the specified location, with a scaling
         * factor applied.
         */
        void drawStringScale(const std::string & str, GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale) const;

        /**
         * Converts the string, drawn at the specified scale, to a valid SVG file.
         * Each rectangle in each character becomes a <polygon> element.
         * Note that this is note rendered properly by browsers, so it is useful to
         * re-export it with inkscape. Also, merging the characters with the union
         * operation, into paths, further removes SVG rendering issues.
         */
        std::string convertStringToSvg(const std::string & str, GLfloat xScale, GLfloat yScale) const;

        // ------------------ Floor methods -----------------------------------

        /**
         * This acts exactly the same as drawCStringScale, but the x and y coordinates are automatically
         * rounded down to the nearest whole number. This should be used instead of drawCStringScale for UI stuff,
         * since this method ensures that text is always drawn consistently, even if given a non-whole position.
         */
        void drawFloorCStringScale(const char * string, GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale) const;

        /**
         * This acts exactly the same as drawStringScale, but the x and y coordinates are automatically
         * rounded down to the nearest whole number. This should be used instead of drawStringScale for UI stuff,
         * since this method ensures that text is always drawn consistently, even if given a non-whole position.
         */
        void drawFloorStringScale(const std::string & str, GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale) const;

        // ------------------- Config methods ---------------------------------

        /**
         * This gets the width of the characters drawn by this font.
         */
        GLfloat getCharWidth(void) const;

        /**
         * This gets the height of the characters drawn by this font.
         */
        GLfloat getCharHeight(void) const;

    };

}

#endif
