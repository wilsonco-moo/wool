/*
 * TextureRegion.cpp
 *
 *  Created on: 18 Jul 2018
 *      Author: wilson
 */

#include "TextureRegion.h"

namespace wool {

    TextureRegion::TextureRegion(void) :
        u1(0), v1(0),
        u2(0), v2(0),
        u3(0), v3(0),
        u4(0), v4(0) {}

    TextureRegion::TextureRegion(
        GLfloat u1, GLfloat v1,
        GLfloat u2, GLfloat v2,
        GLfloat u3, GLfloat v3,
        GLfloat u4, GLfloat v4
    ) : u1(u1), v1(v1),
        u2(u2), v2(v2),
        u3(u3), v3(v3),
        u4(u4), v4(v4) {}

    TextureRegion::TextureRegion(
        GLfloat x, GLfloat y,
        GLfloat width, GLfloat height
    ) : TextureRegion(
        x, y,
        x + width, y,
        x + width, y + height,
        x, y + height
    ) {}

    TextureRegion::TextureRegion(
        int x, int y, int width, int height,
        std::pair<unsigned int, unsigned int> area
    ) : TextureRegion(
        ((GLfloat)x          ) / ((GLfloat)area.first), ((GLfloat)y           ) / ((GLfloat)area.second),
        ((GLfloat)(x + width)) / ((GLfloat)area.first), ((GLfloat)y           ) / ((GLfloat)area.second),
        ((GLfloat)(x + width)) / ((GLfloat)area.first), ((GLfloat)(y + height)) / ((GLfloat)area.second),
        ((GLfloat)x          ) / ((GLfloat)area.first), ((GLfloat)(y + height)) / ((GLfloat)area.second)
    ) {}

    TextureRegion::~TextureRegion(void) {
    }
}
