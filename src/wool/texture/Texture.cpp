/*
 * Texture.cpp
 *
 *  Created on: 18 Jul 2018
 *      Author: wilson
 */

#include "Texture.h"

#include <iostream>
#include <cstddef>
#include <limits>

#include "lodepng/lodepng.h"


namespace wool {

    // The default width and height to use for textures, if we don't know the size of them.
    const int Texture::DEFAULT_WIDTH = 32,
              Texture::DEFAULT_HEIGHT = 32;


    Texture::Texture(void) :
        textureId(0),
        width(DEFAULT_WIDTH), // Avoid divide by zero errors if region is called
        height(DEFAULT_HEIGHT),
        textureMinScaling(GL_NEAREST),
        textureMagScaling(GL_NEAREST),
        haveRamCopy(false),
        haveVRamCopy(false),
        ramCopyOfTexture(NULL) {
    }

    Texture::Texture(const char * filename, bool keepRamCopy, bool keepVRamCopy, GLint minScaling, GLint magScaling) :
        Texture() {
        // Automatically load in this constructor.
        load(filename, keepRamCopy, keepVRamCopy, minScaling, magScaling);
    }

    Texture::Texture(const std::string & filename, bool keepRamCopy, bool keepVRamCopy, GLint minScaling, GLint magScaling) :
        Texture(filename.c_str(), keepRamCopy, keepVRamCopy, minScaling, magScaling) {
    }

    Texture::Texture(unsigned int requiredWidth, unsigned int requiredHeight, bool keepRamCopy, bool keepVRamCopy, GLint minScaling, GLint magScaling) :
        Texture() {
        load(requiredWidth, requiredHeight, keepRamCopy, keepVRamCopy, minScaling, magScaling);
    }

    Texture::~Texture(void) {
        // Unload the copy in VRAM if we have one.
        unloadVRamCopy();
        // Then free the copy in RAM memory, if we have one.
        if (haveRamCopy) {
            free(ramCopyOfTexture);
            // Set this to false, we no longer have a copy in RAM memory.
            haveRamCopy = false;
        }
    }


    bool Texture::load(const std::string & filename, bool keepRamCopy, bool keepVRamCopy, GLint minScaling, GLint magScaling) {
        return load(filename.c_str(), keepRamCopy, keepVRamCopy, minScaling, magScaling);
    }

    bool Texture::load(const char * filename, bool keepRamCopy, bool keepVRamCopy, GLint minScaling, GLint magScaling) {
        std::cout << "Loading texture: " << filename << " ...\n";
        // Do nothing, if we already have a texture loaded.
        if (haveVRamCopy || haveRamCopy) return false;

        // Do nothing if we are told to load neither a RAM nor a VRAM copy.
        // Return true since we cannot fail to do nothing!
        if ((!keepRamCopy) && (!keepVRamCopy)) return true;

        // Set the initial values of width, height and texture id.
        width = 0; height = 0; textureId = 0;

        // Set our minScaling and magScaling from the values provided.
        textureMinScaling = minScaling;
        textureMagScaling = magScaling;

        unsigned error = lodepng_decode32_file(&ramCopyOfTexture, &width, &height, filename);

        if (error) {
            // If the image failed to load for whatever reason, such as the file not existing,
            // print a warning.
            std::cerr << "WARNING: Texture: " << "Failed to load texture: " << filename << '\n';
            // Then free anything that may have been loaded,
            free(ramCopyOfTexture);
            // .. and return false.
            return false;
        } else {
            // If successful, set this to true as we now have a copy of the texture in RAM memory.
            haveRamCopy = true;
        }

        // If we are supposed to keep a copy in VRAM:
        if (keepVRamCopy) {
            std::cout << "Initially loading VRAM copy of " << filename << "...\n";
            // Then load a copy of the texture into VRAM.
            loadVRamCopy();
        }

        // If we are not supposed to keep a RAM copy:
        if (!keepRamCopy) {
            // Then free our ram copy, now the texture is in VRAM.
            free(ramCopyOfTexture);
            // Set this to false, since we no longer have a RAM copy of the texture.
            haveRamCopy = false;
        }
        std::cout << "Finished loading texture " << filename << ".\n";
        return true;
    }

    bool Texture::load(unsigned int requiredWidth, unsigned int requiredHeight, bool keepRamCopy, bool keepVRamCopy, GLint minScaling, GLint magScaling) {

        // Complain if the size is too large to be represented by a size_t (i.e:
        // working out the size in the calloc call would overflow).
        if (requiredWidth > std::numeric_limits<size_t>::max() / requiredHeight) {
            std::cerr << "WARNING: Texture: " << "Failed to allocate data because requested size is too large.\n";
            return false;
        }

        // Do nothing, if we already have a texture loaded.
        if (haveVRamCopy || haveRamCopy) return false;

        // Do nothing if we are told to load neither a RAM nor a VRAM copy.
        // Return true since we cannot fail to do nothing!
        if ((!keepRamCopy) && (!keepVRamCopy)) return true;

        // Set the initial values of width, height and texture id.
        width = requiredWidth; height = requiredHeight; textureId = 0;

        // Set our minScaling and magScaling from the values provided.
        textureMinScaling = minScaling;
        textureMagScaling = magScaling;

        // Allocate the ram copy of the texture. We want to allocate 4 bytes for each pixel.
        ramCopyOfTexture = (unsigned char *)calloc((size_t)requiredWidth * (size_t)requiredHeight, 4);

        // If the allocation failed, complain to the log and return false.
        if (ramCopyOfTexture == NULL) {
            std::cerr << "WARNING: Texture: " << "Failed to allocate data for manual width/height texture load.\n";
            return false;

        // If successful, set this to true as we now have a copy of the texture in RAM memory.
        } else {
            haveRamCopy = true;
        }

        // Load into VRAM if we are required to do so.
        if (keepVRamCopy) {
            loadVRamCopy();
        }

        // If we are not supposed to keep a RAM copy:
        if (!keepRamCopy) {
            // Then free our ram copy, now the texture is in VRAM.
            free(ramCopyOfTexture);
            // Set this to false, since we no longer have a RAM copy of the texture.
            haveRamCopy = false;
        }
        return true;
    }


    /**
     * Assuming the alignment is set correctly, and a valid texture is bound
     * to GL_TEXTURE_2D, this macro sets appropriate texture settings, then
     * uses glTexImage2D to create a texture using the texture data from
     * ramCopyOfTexture. This should be used to initially create the
     * texture, or to update it.
     */
    #define CREATE_TEXTURE_FROM_RAM_COPY                                                                                                    \
        /* Set what to do at the edge of the texture UV outside the 0-1 range. We                                                        */ \
        /* should repeat, as that is the sensible thing to do.                                                                           */ \
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);                                                                       \
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);                                                                       \
                                                                                                                                            \
        /* Set the type of interpolation to use, for scaling up (magScaling) and scaling down (minScaling).                              */ \
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, textureMagScaling);                                                           \
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, textureMinScaling);                                                           \
                                                                                                                                            \
        /* Finally, actually bundle the image data off to the graphics card's vram, so it can be used                                    */ \
        /* as a texture.                                                                                                                 */ \
        glTexImage2D(                                                                                                                       \
            GL_TEXTURE_2D,      /* The texture target. For everything we are using GL_TEXTURE_2D.                                        */ \
            0,                  /* The mipmapping level of detail. Set this to zero as it is of no use for 2d stuff.                     */ \
            GL_RGBA,            /* The texture's internal format. Here we use RGBA.                                                      */ \
            width,              /* The width of the texture. This has been calculated when we loaded the texture with lodepng.           */ \
            height,             /* The height of the texture. This has been calculated when we loaded the texture with lodepng.          */ \
            0,                  /* The width of the texture's border. According to the documentation, this must always be set to zero.   */ \
            GL_RGBA,            /* The format for the textures pixel data. This must match the internal format, so here we use RGBA.     */ \
            GL_UNSIGNED_BYTE,   /* The data type of the pixel data. Our image data is unsigned byte formatted, so use that.              */ \
            ramCopyOfTexture    /* A pointer to the RAM copy of our texture.                                                             */ \
        );



    void Texture::loadVRamCopy(void) {

        // We can only load the VRAM copy if we already have a RAM copy.
        if (haveRamCopy && (!haveVRamCopy)) {

            // Set the correct pixel storage mode, for the subsequent loading operations.
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

            // Generate a texture ID to use for this texture, specifying that we would like to allocate
            // one single texture.
            glGenTextures(1, &textureId);

            // Bind this texture id, so opengl knows which texture id we are using for the subsequent load
            // operations.
            glBindTexture(GL_TEXTURE_2D, textureId);

            // Set appropriate settings for our new texture, and create a texture from the RAM copy.
            CREATE_TEXTURE_FROM_RAM_COPY

            // Set this to true: We now have a texture in VRAM.
            haveVRamCopy = true;

            // Unbind our texture, and reset to the default (no texture).
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }




    void Texture::unloadVRamCopy(void) {
        // If we have a copy in VRAM:
        if (haveVRamCopy) {
            // Use this function to delete it.
            glDeleteTextures(1, &textureId);
            // Set this to false as we no longer have a copy in VRAM.
            haveVRamCopy = false;
            // Set our textureId to zero, (default (no) texture), so when bind is called
            // nothing useful will happen, and opengl won't crap itself.
            textureId = 0;
        }
    }


    void Texture::updateVRamCopy(void) {

        // This can only be done if we have BOTH a RAM copy and a VRAM copy.
        if (haveRamCopy && haveVRamCopy) {

            // Set the correct pixel storage mode, for the subsequent loading operations.
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

            // Bind our texture id, so opengl knows which texture id we are using for the subsequent operations.
            glBindTexture(GL_TEXTURE_2D, textureId);

            // Copy from the RAM copy of the texture. Note that the driver will replace the previous texture
            // here, meaning we don't have to delete it first, as long as we are using the same texture ID.
            CREATE_TEXTURE_FROM_RAM_COPY

            // Unbind our texture, and reset to the default (no texture).
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }


    void Texture::bind(void) const {
        glBindTexture(GL_TEXTURE_2D, textureId);
    }




    TextureRegion Texture::region(int u1, int v1, int u2, int v2, int u3, int v3, int u4, int v4) const {
        return TextureRegion(
            ((GLfloat)u1) / ((GLfloat)width), ((GLfloat)v1) / ((GLfloat)height),
            ((GLfloat)u2) / ((GLfloat)width), ((GLfloat)v2) / ((GLfloat)height),
            ((GLfloat)u3) / ((GLfloat)width), ((GLfloat)v3) / ((GLfloat)height),
            ((GLfloat)u4) / ((GLfloat)width), ((GLfloat)v4) / ((GLfloat)height)
        );
    }

    TextureRegion Texture::region(int x, int y, int width, int height) const {
        return TextureRegion(x, y, width, height, getSize());
    }
}
