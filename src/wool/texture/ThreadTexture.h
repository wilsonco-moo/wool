/*
 * ThreadTexture.h
 *
 *  Created on: 1 Feb 2019
 *      Author: wilson
 */

#ifndef WOOL_TEXTURE_THREADTEXTURE_H_
#define WOOL_TEXTURE_THREADTEXTURE_H_

#include <GL/gl.h>
#include <cstdint>
#include <mutex>
#include <string>

#include <threading/JobStatus.h>
#include "Texture.h"

namespace threading {
    class ThreadQueue;
}

namespace wool {

    /**
     * ThreadTexture is a simple wrapper around the Texture class.
     * The pixel data in ThreadTexture is loaded using a ThreadQueue, allowing lengthy load operations to be completed
     * in a separate thread.
     *
     * Unfortunately the texture cannot be loaded into VRAM in the separate thread, so if autoLoadVRamCopy is specified,
     * the texture will be automatically loaded into VRAM, the first time bind() is called after the texture is loaded.
     *
     * Note: All ThreadQueue method should be called from the same thread.
     */
    class ThreadTexture {
    private:
        // ---------- Communication the loading thread -------

        // A mutex to control access to the following properties.
        std::mutex mutex;
        // This is set by the thread, from within the mutex, from false to true, when the thread finishes loading.
        bool internalThreadFinished;
        // This should be accessed only from the OpenGL thread. If externalThreadFinished is false, internalThreadFinished should be checked
        // from within the mutex. When internalThreadFinished becomes true, externalThreadFinished should be set to true.
        // If externalThreadFinished is true, we can thus assume the thread has finished without checking the mutex.
        bool externalThreadFinished;

        // ---------------------------------------------------

        // Stores whether the load method, (or appropriate constructor) has yet been called.
        // This effectively stores true if we are either currently loading, or have already loaded.
        bool loadCalled;
        // Stores whether we should automatically load a VRAM copy the first time bind is called after we are loaded.
        bool autoLoadVRamCopy;
        // Stores true if we have already auto loaded to VRAM, from autoLoadVRamCopy being set.
        bool haveAutoLoadedVRamCopy;
        // The Texture instance we are using internally.
        Texture internalTexture;
        // The ThreadQueue we are using.
        threading::ThreadQueue * threadQueue;
        // The token from the ThreadQueue.
        void * token;
        // Our job id from the thread queue.
        uint64_t jobId;
    public:

        /**
         * Like with Texture,
         *
         * ThreadTexture tex;
         * tex.load(queue, "image.png", true);
         *
         * ... is equivalent to ...
         *
         * ThreadTexture tex(queue, "image.png", true);
         */
        ThreadTexture(void);

        /**
         * Creates a ThreadTexture, and automatically starts it loading.
         */
        ThreadTexture(threading::ThreadQueue * threadQueue, const char * filename, bool autoLoadVRamCopy, GLint minScaling = GL_NEAREST, GLint magScaling = GL_NEAREST);
        ThreadTexture(threading::ThreadQueue * threadQueue, const std::string & filenames, bool autoLoadVRamCopy, GLint minScaling = GL_NEAREST, GLint magScaling = GL_NEAREST);

        /**
         * When ThreadTexture is destroyed, it will wait until loading has completed before
         * freeing all associated resources.
         */
        virtual ~ThreadTexture(void);

    public:

        /**
         * Starts the ThreadTexture loading.
         * This will do nothing if we have started loading already.
         */
        void load(threading::ThreadQueue * threadQueue, const char * filename, bool autoLoadVRamCopy, GLint minScaling = GL_NEAREST, GLint magScaling = GL_NEAREST);
        void load(threading::ThreadQueue * threadQueue, const std::string & filename, bool autoLoadVRamCopy, GLint minScaling = GL_NEAREST, GLint magScaling = GL_NEAREST);

        /**
         * If we have finished loading:
         *   Tells our internal texture to load it's VRam copy, see Texture::loadVRamCopy. Returns true.
         * Otherwise:
         *   Does nothing, returns false.
         */
        bool loadVRamCopy(void);

        /**
         * If we have finished loading:
         *   Tells our internal texture to unload it's VRam copy, see Texture::unloadVRamCopy. Returns true.
         *   If autoLoadVRamCopy is set, this will automatically reset haveAutoLoadedVRamCopy so that the
         *   texture is automatically reloaded next time bind is called.
         * Otherwise:
         *   Does nothing, returns false.
         */
        bool unloadVRamCopy(void);

        /**
         * If we have finished loading:
         *   Binds our internal texture, see Texture::bind. Returns true.
         *   Note: If autoLoadVRamCopy was set, the first time this is run after we are loaded, Texture::loadVRamCopy will
         *         be called for our internal texture.
         * Otherwise:
         *   Does nothing, returns false.
         */
        bool bind(void);

        /**
         * If we have finished loading:
         *   Generates a new TextureRegion from our internal texture, see Texture::region.
         * Otherwise:
         *   Returns a new default-constructed TextureRegion.
         */
        TextureRegion region(int u1, int v1, int u2, int v2, int u3, int v3, int u4, int v4);

        /**
         * If we have finished loading:
         *   Generates a new TextureRegion from our internal texture, see Texture::region.
         * Otherwise:
         *   Returns a new default-constructed TextureRegion.
         */
        TextureRegion region(int x, int y, int width, int height);

        /**
         * If we have finished loading:
         *   Returns the width of our internal texture.
         * Otherwise:
         *   Returns Texture::DEFAULT_WIDTH.
         */
        int getWidth(void);

        /**
         * If we have finished loading:
         *   Returns the height of our internal texture.
         * Otherwise:
         *   Returns Texture::DEFAULT_HEIGHT.
         */
        int getHeight(void);

        /**
         * If we have finished loading:
         *   Returns true.
         * Otherwise:
         *   Returns false.
         */
        bool hasFinishedLoading(void);

        /**
         * If we have finished loading:
         *   Returns whether our internal texture has loaded successfully, see Texture::isLoaded.
         * Otherwise:
         *   Returns false.
         */
        bool hasFinishedLoadingSuccessfully(void);

        /**
         * If we have finished loading:
         *   Returns the specified pixel from our internal texture, see Texture::getPixelRGBA.
         * Otherwise:
         *   Returns a default constructed RGBA instance.
         */
        RGBA getPixelRGBA(int x, int y);

        /**
         * If we have finished loading:
         *   Returns the specified pixel from our internal texture, see Texture::getPixelInt.
         * Otherwise:
         *   Returns 0.
         */
        uint32_t getPixelInt(int x, int y);

        // ------------------- ThreadTexture specific methods -----------

        /**
         * If we have finished loading:
         *   Returns a pointer to our internal texture. Our internal texture CANNOT NOT BE USED after we are destroyed.
         * Otherwise:
         *   Returns NULL.
         */
        Texture * getInternalTexture(void);

        /**
         * Returns the current status of our loading texture job. See threading::JobStatus.
         */
        threading::JobStatus getLoadJobStatus(void);

        /**
         * Returns the job id of our loading texture job.
         */
        inline uint64_t getLoadJobId(void) {
            return jobId;
        }
    };

}

#endif
