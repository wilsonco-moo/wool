/*
 * Texture.h
 *
 *  Created on: 18 Jul 2018
 *      Author: wilson
 *
 * Internally, the Texture class loads images using LodePNG.
 * https://lodev.org/lodepng/
 * https://github.com/lvandeve/lodepng
 * Copyright (c) 2005-2018 Lode Vandevenne
 */

#ifndef WOOL_TEXTURE_TEXTURE_H_
#define WOOL_TEXTURE_TEXTURE_H_

#include <GL/gl.h>
#include <utility>
#include <cstdint>
#include <string>

#include "../misc/RGBA.h"
#include "TextureRegion.h"

namespace wool {
class TextureRegion;
} /* namespace wool */

namespace wool {


    /**
     * The Texture class represents a texture loaded from a file.
     *
     * Textures MUST NOT BE LOADED before opengl has started, so textures MUST be loaded in
     * in the first room, textures must be loaded in the init() method, NOT the constructor.
     * This is why load() can be called separately from the constructor.
     *
     * The bind() method can be used to make this texture in general use.
     * The region() methods can be used to generate TextureRegions from this texture, (taking into account the width and height).
     *
     * Textures can be loaded into RAM, VRAM or both, then if we have a copy in RAM memory,
     * loadVRamCopy() and unloadVRamCopy() can be used to switch between whether a copy is loaded in
     * VRAM or not. Textures should be unloaded from VRAM if they are not being used.
     */
    class Texture {
    private:

        GLuint textureId; // Note: These are not const, as otherwise it would make
        unsigned int width;        //       loading in the texture in the constructor very awkward.
        unsigned int height;

        // Our scaling
        GLint textureMinScaling;
        GLint textureMagScaling;


        // Whether we have a copy of the texture in VRAM.
        bool haveRamCopy;

        // Whether we have a copy if the texture in RAM.
        bool haveVRamCopy;


        /**
         * This contains the RAM copy of the texture.
         */
        unsigned char * ramCopyOfTexture;

        /**
         * You cannot copy a texture instance, as otherwise resources in VRAM would be duplicated.
         * Hence Texture's copy constructor is private.
         */
        Texture(const Texture & texture);

        /**
         * You cannot copy a texture instance, as otherwise resources in VRAM would be duplicated.
         * Hence Texture's copy assignment operator is private.
         */
        Texture & operator = (const Texture & texture);


    public:

        // The default width and height to use for textures, if we don't know the size of them.
        static const int DEFAULT_WIDTH, DEFAULT_HEIGHT;

        /**
         * Creates a new empty texture.
         */
        Texture(void);

        /**
         * Texture tex("image.png", false, true);
         *
         * is equivalent to doing:
         *
         * Texture tex;
         * tex.load("image.png", false, true);
         */
        Texture(const char * filename, bool keepRamCopy, bool keepVRamCopy, GLint minScaling = GL_NEAREST, GLint magScaling = GL_NEAREST);

        /**
         * Allows std::string to be used for the filename.
         */
        Texture(const std::string & filename, bool keepRamCopy, bool keepVRamCopy, GLint minScaling = GL_NEAREST, GLint magScaling = GL_NEAREST);

        /**
         * Another alternate constructor: This one allows manually
         * creating a texture from a width and height.
         */
        Texture(unsigned int requiredWidth, unsigned int requiredHeight, bool keepRamCopy, bool keepVRamCopy, GLint minScaling = GL_NEAREST, GLint magScaling = GL_NEAREST);



        /**
         * The destructor for this texture.
         * Here the texture is removed from the graphics card's memory, if it has been loaded.
         */
        virtual ~Texture(void);



        /**
         * load
         *
         * Texture class load method. A filename to load from must be specified.
         * Does nothing, and returns false, if we already have a texture loaded.
         *
         * This method allows us to control where the texture is loaded to: RAM, VRAM or both. This is
         * configurable by the keepRamCopy and keepVRamCopy parameters.
         * A copy in RAM memory is required for getPixelRGBA() and getPixelInt(), and a copy in VRAM is
         * required for the texture to actually be drawn.
         *
         * If we have a copy in RAM memory, but not in VRAM, then the loadVRamCopy() method can be used
         * later on. This copies the texture to VRAM. The method unloadVRamCopy() can later be used, to free
         * the copy in VRAM.
         *
         * If both keepRamCopy and keepVRamCopy are set to false, this method will do nothing at all.
         *
         * Parameters:
         *           const char * filename    The filename of the texture to load.
         *                bool keepRamCopy    Whether we should keep a copy in RAM memory. Setting this to true allows getPixelRGBA(),
         *                                    getPixelInt(), loadVRamCopy(), unloadVRamCopy() to be used.
         *               bool keepVRamCopy    Whether we should keep a copy in VRAM. Setting this to true allows the texture to be used for
         *                                    drawing, and bound with the bind method.
         *     GLint minScaling (optional)    The scaling algorithm to use when the texture is scaled down (MINimised).
         *                                    Can be any value from GL_NEAREST, GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR.
         *                                    By default, GL_NEAREST is used, (no interpolation).
         *     GLint magScaling (optional)    The scaling algorithm to use when the texture is scaled up (MAGnified).
         *                                    Can be any value from GL_NEAREST, GL_LINEAR.
         *                                    By default, GL_NEAREST is used, (no interpolation).
         *
         * Return:
         *     Returns true upon success, false upon failure to load the texture.
         *
         *
         */
        bool load(const char * filename, bool keepRamCopy, bool keepVRamCopy, GLint minScaling = GL_NEAREST, GLint magScaling = GL_NEAREST);

        /**
         * An alternate load method, allowing for the filename to be specified as a std::string.
         */
        bool load(const std::string & filename, bool keepRamCopy, bool keepVRamCopy, GLint minScaling = GL_NEAREST, GLint magScaling = GL_NEAREST);

        /**
         * Manually creates a texture using the specified width and height.
         * This load method does not load an image from the disk. Rather, it
         * allocates an appropriately size buffer, full of zeroes.
         * This can fail if we are already loaded, or the allocation fails.
         * All other parameters are the same as the normal load method.
         */
        bool load(unsigned int requiredWidth, unsigned int requiredHeight, bool keepRamCopy, bool keepVRamCopy, GLint minScaling = GL_NEAREST, GLint magScaling = GL_NEAREST);


        /**
         * If we have a copy of the texture in RAM memory, this method copies the texture to VRAM.
         */
        void loadVRamCopy(void);

        /**
         * This method frees the copy of the texture in VRAM. Note: This should not be called if we
         * do not have a copy of the texture in RAM, or we will have destroyed our only copy of the texture.
         */
        void unloadVRamCopy(void);

        /**
         * This method copies the RAM copy of the texture, to the VRAM copy. This is intended to
         * be called after modifying the RAM copy of the texture, (using getRamCopyOfTexture).
         * If we either don't have a RAM copy, or don't have a VRAM copy of the texture, this method
         * does nothing.
         * Note that this is equivalent to (and faster than) calling unloadVRamCopy() then loadVRamCopy().
         */
        void updateVRamCopy(void);


        /**
         * Binds this texture, to make this texture the currently used texture.
         * Will bind to zero, the default empty texture, if the texture is not loaded.
         */
        void bind(void) const;


        /**
         * This generates a texture region from the specified pixel-defined uv area
         * within this texture.
         */
        TextureRegion region(int u1, int v1, int u2, int v2, int u3, int v3, int u4, int v4) const;

        /**
         * This generates a texture region from the specified pixel-defined rectangular area
         * within this texture.
         */
        TextureRegion region(int x, int y, int width, int height) const;


        inline int getWidth(void) const {
            return width;
        }
        inline int getHeight(void) const {
            return height;
        }
        inline std::pair<unsigned int, unsigned int> getSize(void) const {
            return std::pair<unsigned int, unsigned int>(width, height);
        }

        /**
         * Returns whether this texture has been loaded or not.
         */
        inline bool isLoaded(void) const {
            return haveRamCopy || haveVRamCopy;
        }

        /**
         * Returns whether this texture has a RAM copy.
         */
        inline bool getHasRamCopy(void) const {
            return haveRamCopy;
        }

        /**
         * Returns whether this texture has a VRAM copy.
         */
        inline bool getHasVRamCopy(void) const {
            return haveVRamCopy;
        }


        /**
         * Returns a pixel from the texture, as an RGBA instance.
         *
         * THIS CAN ONLY BE DONE IF WE HOLD A RAM COPY OF THE TEXTURE.
         *
         * THIS WILL CAUSE A SEGMENTATION FAULT IF THE keepRamCopyOfTexture PARAMETER IN THE LOAD
         * METHOD OR CONSTRUCTOR IS SET TO FALSE.
         */
        inline RGBA getPixelRGBA(int x, int y) {
            unsigned char * pixel = (unsigned char *)(((uint32_t *)ramCopyOfTexture) + x + y * width);
            return RGBA((int)(*pixel), (int)(*(pixel+1)), (int)(*(pixel+2)), (int)(*(pixel+3)));
        }

        /**
         * Returns a pixel from the texture, as an int value.
         *
         *    The most significant 8 bits represent RED,
         *              the next 8 bits represent GREEN,
         *               the next 8 bits represent BLUE,
         * the least significant 8 bits represent ALPHA.
         *
         * THIS CAN ONLY BE DONE IF WE HOLD A RAM COPY OF THE TEXTURE.
         *
         * THIS WILL CAUSE A SEGMENTATION FAULT IF THE keepRamCopyOfTexture PARAMETER IN THE LOAD
         * METHOD OR CONSTRUCTOR IS SET TO FALSE.
         */
        inline uint32_t getPixelInt(int x, int y) {
            unsigned char * pixel = (unsigned char *)(((uint32_t *)ramCopyOfTexture) + x + y * width);
            return (((uint32_t)*(pixel)) << 24) |
                   (((uint32_t)*(pixel+1)) << 16) |
                   (((uint32_t)*(pixel+2)) << 8) |
                   ((uint32_t)*(pixel+3));
        }

        /**
         * This allows access to, and direct modification of, the ram copy of our texture.
         * Note that this must only be used if we *actually have* a ram copy of the texture.
         */
        inline unsigned char * getRamCopyOfTexture(void) const {
            return ramCopyOfTexture;
        }

        /**
         * Returns the internal OpenGL texture ID for this texture. This is only
         * relevant if the texture is loaded into VRAM.
         */
        inline GLuint getTextureId(void) const {
            return textureId;
        }
    };




    /**
     * This convenience macro draws a texture as 4 coordinates, assuming uv mapping as follows:
     *
     * wool_textureVertex(x1, y1, x2, y2, x3, y3, x4, y4)
     *
     *       x1,y1 ---------- x2,y2
     *         |                |
     *         |                |
     *         |                |
     *         |                |
     *         |                |
     *         |                |
     *       x4,y4 ---------- x3,y3
     */
    #define wool_textureVertex(x1, y1, x2, y2, x3, y3, x4, y4) \
        glTexCoord2f(0.0, 0.0); glVertex2f((x1), (y1));        \
        glTexCoord2f(1.0, 0.0); glVertex2f((x2), (y2));        \
        glTexCoord2f(1.0, 1.0); glVertex2f((x3), (y3));        \
        glTexCoord2f(0.0, 1.0); glVertex2f((x4), (y4));


    /**
     * This convenience macro draws a texture as a rectangle, given by x, y, width, height.
     * The UV mapping is done in the same way as textureVertex.
     */
    #define wool_textureRectangle(x, y, width, height) \
        wool_textureVertex(                            \
            (x), (y),                                  \
            (x) + (width), (y),                        \
            (x) + (width), (y) + (height),             \
            (x), (y) + (height)                        \
        );

}



#endif
