/*
 * ThreadTexture.cpp
 *
 *  Created on: 1 Feb 2019
 *      Author: wilson
 */

#include "ThreadTexture.h"

#include <cstdint>
#include <iostream>

#include <threading/ThreadQueue.h>

namespace wool {

    ThreadTexture::ThreadTexture(void) :
        mutex(),
        internalThreadFinished(false),
        externalThreadFinished(false),
        loadCalled(false),
        autoLoadVRamCopy(false),
        haveAutoLoadedVRamCopy(false),
        internalTexture(),
        threadQueue(NULL),
        token(NULL),
        jobId(0) {
    }

    ThreadTexture::ThreadTexture(threading::ThreadQueue * threadQueue, const char * filename, bool autoLoadVRamCopy, GLint minScaling, GLint magScaling) :
        ThreadTexture(threadQueue, std::string(filename), autoLoadVRamCopy, minScaling, magScaling) {
    }

    ThreadTexture::ThreadTexture(threading::ThreadQueue * threadQueue, const std::string & filename, bool autoLoadVRamCopy, GLint minScaling, GLint magScaling) :
        mutex(),
        internalThreadFinished(false),
        externalThreadFinished(false),
        loadCalled(false),
        haveAutoLoadedVRamCopy(false),
        internalTexture() {
        load(threadQueue, filename, autoLoadVRamCopy, minScaling, magScaling);
    }

    ThreadTexture::~ThreadTexture(void) {
        if (loadCalled) {
            threadQueue->deleteToken(token);
        }
    }


    /**
     * This macro gets whether the load thread has finished.
     * If this sets threadFinished to true, then we can access our internal texture.
     * If this sets threadFinished to false, we MUST NOT ACCESS our internal texture yet.
     */
    #define GET_HAS_THREAD_FINISHED                                                                     \
        bool threadFinished;                                                                            \
        if (externalThreadFinished) {                                                                   \
            /* If the thread is already known to be finished. */                                        \
            threadFinished = true;                                                                      \
        } else {                                                                                        \
            /* If we don't yet know the thread is finished, we must check internalThreadFinished. */    \
            std::lock_guard<std::mutex> lock(mutex);                                                    \
            if (internalThreadFinished) {                                                               \
                externalThreadFinished = true;                                                          \
                threadFinished = true;                                                                  \
            } else {                                                                                    \
                threadFinished = false;                                                                 \
            }                                                                                           \
        }



    void ThreadTexture::load(threading::ThreadQueue * threadQueue, const char * filename, bool autoLoadVRamCopy, GLint minScaling, GLint magScaling) {
        load(threadQueue, std::string(filename), autoLoadVRamCopy, minScaling, magScaling);
    }

    void ThreadTexture::load(threading::ThreadQueue * threadQueue, const std::string & filename, bool autoLoadVRamCopy, GLint minScaling, GLint magScaling) {
        // Do nothing if load has already been called.
        if (loadCalled) return;
        // Set loadCalled to true, so load can never be run again.
        loadCalled = true;
        // Set fields from the load method's parameters.
        this->autoLoadVRamCopy = autoLoadVRamCopy;
        this->threadQueue = threadQueue;
        // Create a token so we can ensure that we are not destroyed before loading the image finishes.
        token = threadQueue->createToken();
        // Load in another thread...
        jobId = threadQueue->add(token, [this, filename, minScaling, magScaling](void) {
            // Call the texture's load method.
            // Don't load into vram, this must be done in the OpenGL thread.
            internalTexture.load(filename, true, false, minScaling, magScaling);
            // Set internalThreadFinished to say we have finished.
            std::lock_guard<std::mutex> lock(mutex);
            internalThreadFinished = true;
        });
        std::cout << "Job added.\n";
    }

    bool ThreadTexture::loadVRamCopy(void) {
        GET_HAS_THREAD_FINISHED
        if (threadFinished) {
            internalTexture.loadVRamCopy();
        }
        return threadFinished;
    }

    bool ThreadTexture::unloadVRamCopy(void) {
        GET_HAS_THREAD_FINISHED
        if (threadFinished) {
            internalTexture.unloadVRamCopy();
            if (autoLoadVRamCopy) {
                haveAutoLoadedVRamCopy = false;
            }
        }
        return threadFinished;
    }

    bool ThreadTexture::bind(void) {
        GET_HAS_THREAD_FINISHED
        if (threadFinished) {
            if (autoLoadVRamCopy && !haveAutoLoadedVRamCopy) {
                haveAutoLoadedVRamCopy = true;
                internalTexture.loadVRamCopy();
            }
            internalTexture.bind();
        }
        return threadFinished;
    }

    TextureRegion ThreadTexture::region(int u1, int v1, int u2, int v2, int u3, int v3, int u4, int v4) {
        GET_HAS_THREAD_FINISHED
        if (threadFinished) {
            return internalTexture.region(u1, v1, u2, v2, u3, v3, u4, v4);
        } else {
            return TextureRegion();
        }
    }

    TextureRegion ThreadTexture::region(int x, int y, int width, int height) {
        GET_HAS_THREAD_FINISHED
        if (threadFinished) {
            return internalTexture.region(x, y, width, height);
        } else {
            return TextureRegion();
        }
    }

    int ThreadTexture::getWidth(void) {
        GET_HAS_THREAD_FINISHED
        if (threadFinished) {
            return internalTexture.getWidth();
        } else {
            return Texture::DEFAULT_WIDTH;
        }
    }

    int ThreadTexture::getHeight(void) {
        GET_HAS_THREAD_FINISHED
        if (threadFinished) {
            return internalTexture.getHeight();
        } else {
            return Texture::DEFAULT_HEIGHT;
        }
    }

    bool ThreadTexture::hasFinishedLoading(void) {
        GET_HAS_THREAD_FINISHED
        return threadFinished;
    }

    bool ThreadTexture::hasFinishedLoadingSuccessfully(void) {
        GET_HAS_THREAD_FINISHED
        if (threadFinished) {
            return internalTexture.isLoaded();
        } else {
            return false;
        }
    }

    RGBA ThreadTexture::getPixelRGBA(int x, int y) {
        GET_HAS_THREAD_FINISHED
        if (threadFinished) {
            return internalTexture.getPixelRGBA(x, y);
        } else {
            return RGBA();
        }
    }

    uint32_t ThreadTexture::getPixelInt(int x, int y) {
        GET_HAS_THREAD_FINISHED
        if (threadFinished) {
            return internalTexture.getPixelInt(x, y);
        } else {
            return 0;
        }
    }

    Texture * ThreadTexture::getInternalTexture(void) {
        GET_HAS_THREAD_FINISHED
        if (threadFinished) {
            return &internalTexture;
        } else {
            return NULL;
        }
    }

    threading::JobStatus ThreadTexture::getLoadJobStatus(void) {
        if (loadCalled) {
            return threadQueue->getJobStatus(jobId);
        } else {
            return threading::JobStatus::waiting;
        }
    }
}
