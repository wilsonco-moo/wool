/*
 * RGBA.cpp
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#include "../../wool/misc/RGBA.h"

namespace wool {

    RGBA::RGBA() {
        red = 0.0f;
        green = 0.0f;
        blue = 0.0f;
        alpha = 0.0f;
    }

    RGBA::RGBA(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) : red(red), green(green), blue(blue), alpha(alpha) {}
    RGBA::RGBA(int red, int green, int blue, int alpha) : RGBA(((GLfloat)red)/255.0f, ((GLfloat)green)/255.0f, ((GLfloat)blue)/255.0f, ((GLfloat)alpha)/255.0f) {}
    RGBA::RGBA(uint32_t colour) : RGBA((int)((colour >> 24) & 255), (int)((colour >> 16) & 255), (int)((colour >> 8) & 255), (int)(colour & 255)) {}


    // ---------- Preset colours -------------

    const RGBA RGBA::WHITE      (255, 255, 255, 255),
               RGBA::LIGHT_GREY (192, 192, 192, 255),
               RGBA::GREY       (128, 128, 128, 255),
               RGBA::DARK_GREY  (64, 64, 64, 255),
               RGBA::BLACK      (0, 0, 0, 255),
               RGBA::RED        (255, 0, 0, 255),
               RGBA::PINK       (255, 175, 175, 255),
               RGBA::ORANGE     (255, 200, 0, 255),
               RGBA::YELLOW     (255, 255, 0, 255),
               RGBA::GREEN      (0, 255, 0, 255),
               RGBA::MAGENTA    (255, 0, 255, 255),
               RGBA::CYAN       (0, 255, 255, 255),
               RGBA::BLUE       (0, 0, 255, 255),
               RGBA::BEIGE      (245, 245, 220, 255),
               RGBA::BROWN      (165, 42, 42, 255),
               RGBA::DARK_RED   (139, 0, 0, 255),
               RGBA::DARK_GREEN (0, 100, 0, 255),
               RGBA::DARK_BLUE  (0, 0, 139, 255),
               RGBA::GOLD       (255, 215, 0, 255),
               RGBA::INDIGO     (75, 0, 130, 255),
               RGBA::LIGHT_RED  (255, 192, 192, 255),
               RGBA::LIGHT_GREEN(144, 238, 144, 255),
               RGBA::LIGHT_BLUE (173, 216, 230, 255),
               RGBA::PURPLE     (128, 0, 128, 255),
               RGBA::VIOLET     (238, 130, 238, 255);
}
