/*
 * RGB.h
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#ifndef WOOL_MISC_RGB_H_
#define WOOL_MISC_RGB_H_

#include <GL/gl.h>
#include <cstdint>


// If using windoze, undefine the RGB macro from the windows.h header, as this conflicts with wool::RGB.
#ifdef _WIN32
    #undef RGB
#endif



namespace wool {

    /**
     * This class allows a colour stored as three GLfloat values: red, green, blue.
     * The macro wool_getRGB allows the class to be used with functions that expect three GLfloat values as a colour.
     * The macro wool_setColourRGB allows a convenient shortcut for glColor3f, using an RGB instance/reference.
     *
     * An RGB instance can be created with three 0-1 floats, or three 0-255 integers.
     * Integers will be automatically converted to floats.
     */
    class RGB {
    public:
        GLfloat red;
        GLfloat green;
        GLfloat blue;

        RGB();
        RGB(GLfloat red, GLfloat green, GLfloat blue);
        RGB(int red, int green, int blue);
        RGB(uint32_t colour);

        /**
         * Generates an int representation of this RGB instance, where:
         *
         *    The most significant 8 bits represent RED,
         *              the next 8 bits represent GREEN,
         *               the next 8 bits represent BLUE,
         * the least significant 8 bits represent ALPHA, automatically set to 255 (opaque).
         */
        inline uint32_t intValue(void) const {
            return ((uint32_t)(red   * 255.0f)) << 24 |
                   ((uint32_t)(green * 255.0f)) << 16 |
                   ((uint32_t)(blue  * 255.0f)) << 8  |
                   255;
        }

        // ---------- Preset colours -------------

        const static RGB WHITE,
                         LIGHT_GREY,
                         GREY,
                         DARK_GREY,
                         BLACK,
                         RED,
                         PINK,
                         ORANGE,
                         YELLOW,
                         GREEN,
                         MAGENTA,
                         CYAN,
                         BLUE,
                         BEIGE,
                         BROWN,
                         DARK_RED,
                         DARK_GREEN,
                         DARK_BLUE,
                         GOLD,
                         INDIGO,
                         LIGHT_RED,
                         LIGHT_GREEN,
                         LIGHT_BLUE,
                         PURPLE,
                         VIOLET;

        // ---------------------------------------
    };


    #define wool_getRGB(rgbInstance) (rgbInstance).red, (rgbInstance).green, (rgbInstance).blue
    #define wool_setColourRGB(rgbInstance) glColor3f(wool_getRGB(rgbInstance));

}

#endif
