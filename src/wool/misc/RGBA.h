/*
 * RGBA.h
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#ifndef WOOL_MISC_RGBA_H_
#define WOOL_MISC_RGBA_H_

#include <GL/gl.h>
#include <cstdint>

namespace wool {

    /**
     * This class allows a colour stored as four GLfloat values: red, green, blue, alpha.
     * The macro wool_getRGBA allows the class to be used with functions that expect four GLfloat values as a colour.
     * The macro wool_setColourRGBA allows a convenient shortcut for glColor4f, using an RGBA instance/reference.
     *
     * An RGBA instance can be created with four 0-1 floats, or four 0-255 integers.
     * Integers will be automatically converted to floats.
     */
    class RGBA {
    public:
        GLfloat red;
        GLfloat green;
        GLfloat blue;
        GLfloat alpha;

        RGBA();
        RGBA(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
        RGBA(int red, int green, int blue, int alpha);
        RGBA(uint32_t colour);

        /**
         * Generates an int representation of this RGBA instance, where:
         *
         *    The most significant 8 bits represent RED,
         *              the next 8 bits represent GREEN,
         *               the next 8 bits represent BLUE,
         * the least significant 8 bits represent ALPHA.
         */
        inline uint32_t intValue(void) const {
            return ((uint32_t)(red   * 255.0f)) << 24 |
                   ((uint32_t)(green * 255.0f)) << 16 |
                   ((uint32_t)(blue  * 255.0f)) << 8  |
                   ((uint32_t)(alpha * 255.0f));
        }

        // ---------- Preset colours -------------

        const static RGBA WHITE,
                          LIGHT_GREY,
                          GREY,
                          DARK_GREY,
                          BLACK,
                          RED,
                          PINK,
                          ORANGE,
                          YELLOW,
                          GREEN,
                          MAGENTA,
                          CYAN,
                          BLUE,
                          BEIGE,
                          BROWN,
                          DARK_RED,
                          DARK_GREEN,
                          DARK_BLUE,
                          GOLD,
                          INDIGO,
                          LIGHT_RED,
                          LIGHT_GREEN,
                          LIGHT_BLUE,
                          PURPLE,
                          VIOLET;

        // ---------------------------------------
    };


    #define wool_getRGBA(rgbInstance) (rgbInstance).red, (rgbInstance).green, (rgbInstance).blue, (rgbInstance).alpha
    #define wool_setColourRGBA(rgbInstance) glColor4f(wool_getRGBA(rgbInstance));

}

#endif
