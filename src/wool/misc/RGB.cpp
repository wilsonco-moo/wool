/*
 * RGB.cpp
 *
 *  Created on: 17 Jul 2018
 *      Author: wilson
 */

#include "RGB.h"

namespace wool {

    RGB::RGB() {
        red = 0.0f;
        green = 0.0f;
        blue = 0.0f;
    }

    RGB::RGB(GLfloat red, GLfloat green, GLfloat blue) : red(red), green(green), blue(blue) {}
    RGB::RGB(int red, int green, int blue) : RGB(((GLfloat)red)/255.0f, ((GLfloat)green)/255.0f, ((GLfloat)blue)/255.0f) {}
    RGB::RGB(uint32_t colour) : RGB((int)((colour >> 24) & 255), (int)((colour >> 16) & 255), (int)((colour >> 8) & 255)) {}


    // ---------- Preset colours -------------

    const RGB RGB::WHITE      (255, 255, 255),
              RGB::LIGHT_GREY (192, 192, 192),
              RGB::GREY       (128, 128, 128),
              RGB::DARK_GREY  (64, 64, 64),
              RGB::BLACK      (0, 0, 0),
              RGB::RED        (255, 0, 0),
              RGB::PINK       (255, 175, 175),
              RGB::ORANGE     (255, 200, 0),
              RGB::YELLOW     (255, 255, 0),
              RGB::GREEN      (0, 255, 0),
              RGB::MAGENTA    (255, 0, 255),
              RGB::CYAN       (0, 255, 255),
              RGB::BLUE       (0, 0, 255),
              RGB::BEIGE      (245, 245, 220),
              RGB::BROWN      (165, 42, 42),
              RGB::DARK_RED   (139, 0, 0),
              RGB::DARK_GREEN (0, 100, 0),
              RGB::DARK_BLUE  (0, 0, 139),
              RGB::GOLD       (255, 215, 0),
              RGB::INDIGO     (75, 0, 130),
              RGB::LIGHT_RED  (255, 192, 192),
              RGB::LIGHT_GREEN(144, 238, 144),
              RGB::LIGHT_BLUE (173, 216, 230),
              RGB::PURPLE     (128, 0, 128),
              RGB::VIOLET     (238, 130, 238);
}
